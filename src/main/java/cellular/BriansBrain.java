package cellular;

import datastructure.IGrid;
import datastructure.CellGrid;
import java.util.Random;


public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();

    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}        

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    
    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();

		for(int i = 0; i < this.numberOfRows(); i++){
			for(int j = 0; j < this.numberOfColumns(); j++){
				CellState stateOfCell = getNextCell(i, j);
				nextGeneration.set(i, j, stateOfCell);
			}
		}
		currentGeneration = nextGeneration;
	}
        

    @Override
    public CellState getNextCell(int row, int col) {
        int aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
        CellState currentState = getCellState(row, col);
        if(currentState == CellState.ALIVE){
            return CellState.DYING;
        }
        else if(currentState == CellState.DYING){
            return CellState.DEAD;
        }
        else{
            if (aliveNeighbours == 2){
                return CellState.ALIVE;
            }
            else{
                return CellState.DEAD;
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }

    private int countNeighbors(int row, int col, CellState state) {
		int neighboursStateInt = 0;
		for (int i = row-1; i<=row+1; i++){
			for (int j = col-1; j <=col+1; j++){
				if(i < 0 || j < 0 || i >= numberOfRows() || j >= numberOfColumns() || (i == row && j == col)){
					continue;
				}
				else if(getCellState(i, j) == state){
					neighboursStateInt++;
				}
			}
		}
		return neighboursStateInt;
    }
    
}
