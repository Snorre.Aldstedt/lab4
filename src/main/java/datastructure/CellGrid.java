package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState[][] grid;
    private CellState initialState;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];

        for(CellState[] row : grid){
            for(CellState state : row){
                state = this.initialState;
            }
        }
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return this.rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if(OutOfBounds(row, column)){
            throw new IndexOutOfBoundsException("Row or Column index is out of bounds");
        }
        else{
            this.grid[row][column] = element;
        }
            
    
        
    }

    @Override
    public CellState get(int row, int column) {
        if(OutOfBounds(row, column)){
            throw new IndexOutOfBoundsException("Row or Column index is out of bounds");
        }
        // TODO Auto-generated method stub
        else {
            return this.grid[row][column];
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid newGrid = new CellGrid(numRows(), numColumns(), this.initialState);
        for(int i = 0; i < numRows(); i++){
            for(int j = 0; j < numColumns(); j++){
                CellState state = this.get(i, j);
                newGrid.set(i, j, state);
            }
        }
        return newGrid;
    }

    public boolean OutOfBounds(int row, int column){
        if (row > this.rows || row < 0 || column > this.columns || column < 0){
            return true;
        }
        else{
            return false;
        }
    }
    
}
