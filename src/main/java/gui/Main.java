package gui;

import cellular.CellAutomaton;
import cellular.GameOfLife;
import cellular.BriansBrain;

public class Main {

	public static void main(String[] args) {
		//CellAutomaton ca = new GameOfLife(200,200);
		CellAutomaton ca = new BriansBrain(200, 200);
		CellAutomataGUI.run(ca);
	}

}
